from pro_football_reference_web_scraper import player_game_log as p
from pro_football_reference_web_scraper import team_game_log as t

# player_log = p.get_player_game_log(player = 'Josh Allen', position = 'QB', season = 2022)
# print(player_log)

game_log = t.get_team_game_log(team = 'Kansas City Chiefs', season = 2022)
print(game_log)